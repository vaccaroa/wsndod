/*************************************************************
 * Adding an implementation of a vibration sensor
 *************************************************************
 * Author		 : Allen Thomas Varghese<vargheat@tcd.ie>,
 * 				   Alessandro Vaccaro<vaccaro@tcd.ie>
 * Created Date	 : 26-May-2014
 * Last Modified : 26-May-2014
 *************************************************************
 */
#ifndef __VIBRATION_SENSOR_H__
#define __VIBRATION_SENSOR_H__

#include "lib/sensors.h"

extern const struct sensors_sensor vibration_sensor;

#define VIBRATION_SENSOR "Vibration_Sensor"

#endif
