/*************************************************************
 * Node with local outlier detection capability
 *************************************************************
 * Author		 : Allen Thomas Varghese<vargheat@tcd.ie>,
 * 				   Alessandro Vaccaro<vaccaro@tcd.ie>
 * Created Date	 : 24-May-2014
 * Last Modified : 26-May-2014
 *************************************************************
 */
#include "contiki.h"
#include "net/rime.h"
#include "dev/button-sensor.h"
#include "dev/leds.h"
#include <stdio.h>


// Allen - Start
#include "vibration-sensor.h"
#include <math.h>
#define RING_BUFFER_SIZE 10
static int data[RING_BUFFER_SIZE];
static uint bufferPosition = 0, tick_counter = 0;
static float mean = 0.0f, stddev = 0.0f, median = 0.0f, mad = 0.0f;

static struct ctimer c;
// Time delay of 5 seconds
#define SENSOR_DATA_FETCH_TIMEOUT 5 * CLOCK_SECOND;

/* Calculate Mean */
static float
reCalculateMean(int valArray[])
{
	int i;
	float sum = 0.0f;
	for(i=0;i<bufferPosition;i++)
	{
		sum += valArray[i];
	}
	return sum/(bufferPosition+1);
}


/* Calculate standard deviation */
static float
reCalculateStdDev(float mean, int valArray[])
{
	int i;
	float sum = 0.0f;
	for(i=0;i<bufferPosition;i++)
	{
		// Calculate sum of squares of deviation from the mean
		sum += pow(valArray[i] - mean, 2);
	}
	return sqrt(sum/(bufferPosition+1));
}


/* Calculate Median */
static float
reCalculateMedian(int valArray[])
{
	int c, d, swap;
	float retVal = 0.0f;
	
	// Bubble Sort
	for (c = 0 ; c < ( bufferPosition - 1 ); c++)
	{
    	for (d = 0 ; d < bufferPosition - c - 1; d++)
    	{
    		/* Sorting in ascending order */
    		if (valArray[d] > valArray[d+1])
    		{
    		    swap       	 	= valArray[d];
    		    valArray[d]  	= valArray[d+1];
    		    valArray[d+1] 	= swap;
    		}
    	}
  	}
  	
  	// Checking for even number of elements
  	if((bufferPosition+1)%2 == 0)
  	{
  		retVal = (valArray[(bufferPosition+1)/2] + valArray[(bufferPosition-1)/2])/2;
  	} else {
  		retVal = valArray[bufferPosition/2];
  	}
  	return retVal;
}


/* Calculate Median Absolute Deviation(MAD) */
static float
reCalculateMAD(float median, int valArray[])
{
	int i;
	int tempArray[RING_BUFFER_SIZE];
	
	for(i=0;i<bufferPosition;i++)
	{
		// Calculate absolute value for deviation from the mean
		tempArray[i] = abs(valArray[i] - median);
	}
	
	// MAD => Median of absolute deviations
	return reCalculateMedian(tempArray);
}


/* Process data from vibration sensor */
static void
processSensorData(int val)
{
	// Insert data at current position
	// If reached the end of the buffer, then reset index to zero
	data[(bufferPosition == RING_BUFFER_SIZE-1)?0:bufferPosition++] = val;
	
	mean	= reCalculateMean(data);
	stddev	= reCalculateStdDev(mean,data);
	
	// median	= reCalculateMedian(data);
	// mad		= reCalculateMAD(median, data);
}


/* Callback function whenever time delay event fires */
static void
sensorCallback(void *in)
{
	// Read data from sensor
	processSensorData(24);
}
// Allen - End


/*---------------------------------------------------------------------------*/
PROCESS(example_unicast_process, "Example unicast");
AUTOSTART_PROCESSES(&example_unicast_process);
/*---------------------------------------------------------------------------*/
static void
recv_uc(struct unicast_conn *c, const rimeaddr_t *from)
{
  printf("unicast message received from %d.%d\n",
	 from->u8[0], from->u8[1]);
}
static const struct unicast_callbacks unicast_callbacks = {recv_uc};
static struct unicast_conn uc;
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(example_unicast_process, ev, data)
{
//  PROCESS_EXITHANDLER(unicast_close(&uc););
  
  /*
  PROCESS_BEGIN();

  unicast_open(&uc, 146, &unicast_callbacks);

  while(1) {
    static struct etimer et;
    rimeaddr_t addr;
    
    etimer_set(&et, CLOCK_SECOND);
    
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

    packetbuf_copyfrom("Hello", 5);
    addr.u8[0] = 1;
    addr.u8[1] = 0;
    if(!rimeaddr_cmp(&addr, &rimeaddr_node_addr)) {
      unicast_send(&uc, &addr);
    }
  }
  */
  
  PROCESS_BEGIN();
  ctimer_set(&c, SENSOR_DATA_FETCH_TIMEOUT, &sensorCallback, (void *) NULL);
  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
